# README #

JSouthPark - OVRT project

### How do I get set up? ###

* Install Java 1.8 (requiered for lambdas)
* Install Maven
* Install Eclipse
* Configure Eclipse for Java 1.8
* Checkout repo
* In Eclipse choose Import -> Existing Maven project -> Select pom.xml in chooser.
* Ready to go

### Running tests ###

* Using Eclipse, choose the test you want to run, and press "Play" icon.
* Using command-line maven build, tests run automatically.

### Contribution guidelines ###

* Write tests (to your code)
* Write tests (to others code)
* Before commiting, please check all the tests are succeeded
* Before commiting, please check that code can be compiled (via Eclipse or Maven build)
* Readable and understandable code (ofc)

### Who do I talk to? ###

* Repo owner or admin
* Whoever you want to

### Design patterns ###

* Composite (entity package, in IFigure.java, FigureElement.java, Figure.java, etc.)
* Visitor (visitor package)
* Object pool (pool package)
* Builder (entity package, in FigureBuilder.java)
* Flyweight (controller package, in Controller.getBodyPartPoolByBodyPart())
* Strategy (strategy an io packages, in FigureSaver.java, FigureGenerator.java, etc.)
* Adapter (in Controller.loadFigureFromFile(String) - FigureLoader-JsonReader)
* Command (in commands and controller packages, in PreviousBodyPartCommand.java, ScaleCommand.java, etc.)
* Observer (observer, gui and entity packages, in Figure.java, FigureImagePanel.java, etc.)
* Singleton (gui package, in SwingGui.java)
* State (drawing package, in Drawable.java, DrawCircle.java, DrawPoint.java, DrawRectangle.java, etc.)
* Template method (drawing package, in Drawable.concreteDrawShape(), DrawCircle.drawShape(), etc.)