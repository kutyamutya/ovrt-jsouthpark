package southpark.figure.commands;

import southpark.figure.entity.FigureElement;
import southpark.figure.enums.Command;

public class ScaleCommand implements ICommand {
	
	private FigureElement figureElement;
	private Command action;
	private int num;

	/**
	 * Class constructor.
	 */
	public ScaleCommand(FigureElement figureElement, Command action, int num) {
		this.figureElement = figureElement;
		this.action = action;
		this.num = num;
	}

	/**
	 * Execute command.
	 */
	public void execute() {
		switch (action) {
			case SCALE_UP:
			case SCALE_DOWN:
				figureElement.scaleElementImageSize(action, num);
				break;
				
			default:
				break;
		}
	}

	/**
	 * Undo command.
	 */
	public void undo() {
		switch (action) {
			case SCALE_UP:
				figureElement.scaleElementImageSize(Command.SCALE_DOWN, num);
				break;
				
			case SCALE_DOWN:
				figureElement.scaleElementImageSize(Command.SCALE_UP, num);
				break;
				
			default:
				break;
		}
	}

}
