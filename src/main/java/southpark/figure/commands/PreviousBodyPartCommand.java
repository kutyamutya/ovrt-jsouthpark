package southpark.figure.commands;

import southpark.figure.entity.Figure;
import southpark.figure.pool.BodyPartPool;

public class PreviousBodyPartCommand implements ICommand {

	private BodyPartPool partPool;
	private Figure figure;

	/**
	 * Class constructor.
	 */
	public PreviousBodyPartCommand(Figure figure, BodyPartPool pool) {
		this.figure = figure;
		partPool = pool;
	}

	/**
	 * Execute command.
	 */
	public void execute() {
		figure.add(partPool.getType(), partPool.getPrevious());
	}

	/**
	 * Undo command.
	 */
	public void undo() {
		figure.add(partPool.getType(), partPool.getNext());
	}

}
