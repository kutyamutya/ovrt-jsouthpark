package southpark.figure.strategy;

/**
 * Generate figure's strategy
 */
public interface IGenerateFigureStrategy {
	
	/**
	 * Generate operation.
	 */
	public abstract boolean generate();

}
