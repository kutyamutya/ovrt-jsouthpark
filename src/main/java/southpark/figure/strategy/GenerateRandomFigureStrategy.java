package southpark.figure.strategy;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import southpark.figure.controller.Controller;
import southpark.figure.entity.Figure;
import southpark.figure.entity.IFigure;
import southpark.figure.enums.BodyPart;
import southpark.figure.gui.SwingGui;
import southpark.figure.pool.BodyPartPool;
import southpark.figure.visitors.RandomBodyPartPoolVisitor;

/**
 * Random generator
 */
public class GenerateRandomFigureStrategy implements IGenerateFigureStrategy {
	
	private Figure figure;
	private Controller controller;
	private SwingGui gui;
	
	/**
	 * Class constructor.
	 */
	public GenerateRandomFigureStrategy(Figure figure, Controller controller, SwingGui gui) {
		this.figure = figure;
		this.controller = controller;
		this.gui = gui;
	}

	/**
	 * Generate operation.
	 */
	public boolean generate() {
		
		List<BodyPartPool> bodyPartPools = new ArrayList<BodyPartPool>();
		for (BodyPart bodyPart : BodyPart.values()) {
			if (gui.getCheckboxes().get(bodyPart).isSelected()) {
				bodyPartPools.add(controller.getBodyPartPoolByBodyPart(bodyPart));
			}
		}
		
		RandomBodyPartPoolVisitor bppv = new RandomBodyPartPoolVisitor();
		bodyPartPools.forEach((bodyPartPool) -> {
			bodyPartPool.accept(bppv);
		});
		
		Map<BodyPart, IFigure> bodyParts = bppv.getElements();
		bodyParts.forEach((bodyPart, figureElement) -> {
			figure.add(bodyPart, figureElement);
		});
		
		return true;
	}

}
