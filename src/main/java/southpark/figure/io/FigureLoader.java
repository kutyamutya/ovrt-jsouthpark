package southpark.figure.io;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.cedarsoftware.util.io.JsonReader;

import southpark.figure.entity.Figure;

/**
 * Figure loeder
 */
public class FigureLoader {

	private static final Logger log = Logger.getLogger(FigureLoader.class.getCanonicalName());
	private String filePathString;
	private Figure loadedFigure;
	
	/**
	 * Class constructor.
	 */
	public FigureLoader(String filePathString) {
		this.filePathString = filePathString;
	}

	/**
	 * Load figure from JSON.
	 */
	public Figure loadFigureFromJson() {
		if(filePathString.isEmpty()){
			return null;
		}
		
		String json;
		try {
			json = FileUtils.readFileToString(new File(filePathString));
			Figure figureFromJson = (Figure) JsonReader.jsonToJava(json);
			figureFromJson.initElementImage();
			loadedFigure = figureFromJson;
			return loadedFigure;
		} catch (IOException e) {
			log.error("Loading from file failed: " + e);
			return null;
		}
	}

	/**
	 * Get loaded figure.
	 */
	public Figure getLoadedFigure() {
		return loadedFigure;
	}

	/**
	 * Set loaded figure.
	 */
	public void setLoadedFigure(Figure loadedFigure) {
		this.loadedFigure = loadedFigure;
	}

	/**
	 * Get file path string.
	 */
	public String getFilePathString() {
		return filePathString;
	}

	/**
	 * Set file path string.
	 */
	public void setFilePathString(String filePathString) {
		this.filePathString = filePathString;
	}

}
