package southpark.figure.io;

import southpark.figure.strategy.ISaveStrategy;

/**
 * Figure saver
 */
public class FigureSaver {
	
	private ISaveStrategy behavior;
	
	/**
	 * Class constructor.
	 */
	public FigureSaver(ISaveStrategy behaviour) {
		super();
		this.behavior = behaviour;
	}

	/**
	 * Get behavior.
	 */
	public ISaveStrategy getBehavior() {
		return behavior;
	}

	/**
	 * Set behavior.
	 */
	public void setBehavior(ISaveStrategy behaviour) {
		this.behavior = behaviour;
	}

	/**
	 * Concrete save.
	 */
	public boolean concreteSave(String fileName) {
		behavior.setFileName(fileName);

		if (!behavior.checkExtension()) {
			behavior.modifyExtension();
		}

		return behavior.save();
	}

}
