package southpark.figure.enums;

/**
 * Commands for Undo-Redo
 */
public enum Command {
	
	LEFT, RIGHT, UP, DOWN, SCALE_UP, SCALE_DOWN
	
}