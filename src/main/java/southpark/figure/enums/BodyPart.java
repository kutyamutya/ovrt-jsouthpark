package southpark.figure.enums;

import java.io.File;
import java.util.Locale;

import org.apache.log4j.Logger;

import southpark.figure.controller.Controller;
import southpark.figure.utils.Constants;

/**
 * Body parts representation
 */
public enum BodyPart {

	PROPS, HAT, HAIR, EYES, MOUTH, HANDS, LEGS, SHOES, ACCESSORIES, SHIRT, SKIN, BACKGROUND;

	/**
	 * Create directories.
	 */
	public static void createDirs() {
		for (BodyPart bodyPart : BodyPart.values()) {
			File theDir = new File(Constants.RESOURCES_DIR + '/' + bodyPart.toString().toLowerCase(Locale.getDefault()));
			try {
				theDir.mkdir();
			} catch (SecurityException se) {
				Logger.getLogger(Controller.class.getCanonicalName()).error("Directories cannot be created");
			}
		}
	}

	/**
	 * Returns the element by the given index
	 * 
	 * @param index
	 * @return
	 */
	public static BodyPart getElementByIndex(int index) {
		if (index >= 0 && index < values().length) {
			return BodyPart.values()[index];
		} else {
			return BACKGROUND;
		}
	}
};
