package southpark.figure.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import southpark.figure.commands.ICommand;
import southpark.figure.commands.MoveCommand;
import southpark.figure.commands.NextBodyPartCommand;
import southpark.figure.commands.PreviousBodyPartCommand;
import southpark.figure.commands.ScaleCommand;
import southpark.figure.entity.Figure;
import southpark.figure.entity.FigureElement;
import southpark.figure.enums.BodyPart;
import southpark.figure.enums.Command;
import southpark.figure.gui.SwingGui;
import southpark.figure.io.FigureLoader;
import southpark.figure.io.FigureSaver;
import southpark.figure.pool.BodyPartPool;
import southpark.figure.prototypes.FigurePrototypes;
import southpark.figure.strategy.FigureGenerator;
import southpark.figure.strategy.GenerateRandomFigureStrategy;
import southpark.figure.strategy.IGenerateFigureStrategy;
import southpark.figure.strategy.ISaveStrategy;
import southpark.figure.strategy.SaveAsCSVStrategy;
import southpark.figure.strategy.SaveAsFigureStrategy;
import southpark.figure.strategy.SaveAsImageStrategy;
import southpark.figure.strategy.SaveAsMelancholicImageStrategy;
import southpark.figure.utils.Utils;

public class Controller implements IController {

	private static final Logger log = Logger.getLogger(Controller.class.getCanonicalName());

	/**
	 * GUI object
	 */
	private SwingGui gui;

	/**
	 * Visible figure object.
	 */
	private Figure figure;

	/**
	 * Body part's container.
	 */
	private Map<BodyPart, BodyPartPool> bodyPartPools;

	/**
	 * Command container and current index.
	 */
	private List<ICommand> commandHistory;
	private int currentCommandIndex;

	/**
	 * Figure saver object.
	 */
	private FigureSaver saveExecutor;

	/**
	 * Figure generator object.
	 */
	private FigureGenerator figureGenerator;

	/**
	 * Class constructor.
	 */
	public Controller(SwingGui gui) {
		this.gui = gui;
		setFigure(FigurePrototypes.createAndInit());
		bodyPartPools = new HashMap<>();
		commandHistory = new ArrayList<ICommand>();
		currentCommandIndex = -1;
		saveExecutor = new FigureSaver(null);
		figureGenerator = new FigureGenerator(null);
	}

	/**
	 * Set figure.
	 */
	private void setFigure(Figure figure) {
		this.figure = figure;
		this.figure.attachObserver(gui.getFigureImagePanel());
		this.figure.attachObserver(gui.getFigureStatisticsPanel());
		// Figure panel must be the last observer, because of the correct
		// repaint() calls!
		this.figure.attachObserver(gui.getFigurePanel());
		this.figure.notifyObservers();
	}

	/**
	 * Get body part pool by body part.
	 */
	public BodyPartPool getBodyPartPoolByBodyPart(BodyPart part) {
		BodyPartPool result = null;
		if (bodyPartPools.containsKey(part)) {
			result = bodyPartPools.get(part);
		} else {
			result = new BodyPartPool(Utils.getDirectory(part), part);
			bodyPartPools.put(part, result);
		}
		return result;
	}

	/**
	 * Next body part.
	 */
	public void move(BodyPart bodyPart, Command action) {
		BodyPartPool pool = getBodyPartPoolByBodyPart(bodyPart);

		if (pool == null) {
			return;
		}

		ICommand command = null;
		switch (action) {
			case LEFT:
				command = new PreviousBodyPartCommand(figure, pool);
				break;

			case RIGHT:
				command = new NextBodyPartCommand(figure, pool);
				break;

			default:
				log.error("Unknown action");
				return;
		}

		storeAndExecuteCommand(command);
	}

	/**
	 * Save figure image as PNG.
	 */
	public boolean saveImageAsPNG(String fileName) {
		ISaveStrategy strategy = new SaveAsImageStrategy(gui.getFigureImagePanel());
		saveExecutor.setBehavior(strategy);
		return saveExecutor.concreteSave(fileName);
	}

	/**
	 * Save figure image as black & white PNG.
	 */
	public boolean saveImageAsMelancholicPNG(String fileName) {
		ISaveStrategy strategy = new SaveAsMelancholicImageStrategy(gui.getFigureImagePanel());
		saveExecutor.setBehavior(strategy);
		return saveExecutor.concreteSave(fileName);
	}

	/**
	 * Save figure image as figure.
	 */
	public boolean saveImageAsFigure(String fileName) {
		ISaveStrategy strategy = new SaveAsFigureStrategy(figure);
		saveExecutor.setBehavior(strategy);
		return saveExecutor.concreteSave(fileName);
	}

	/**
	 * Save figure image as CSV.
	 */
	public boolean saveImageAsCSV(String fileName) {
		ISaveStrategy strategy = new SaveAsCSVStrategy(figure);
		saveExecutor.setBehavior(strategy);
		return saveExecutor.concreteSave(fileName);
	}

	/**
	 * Load figure from file.
	 */
	public boolean loadFigureFromFile(String loadPathString) {
		figure = new FigureLoader(loadPathString).loadFigureFromJson();
		if (figure != null) {
			setFigure(figure);
			return true;
		}
		return false;
	}

	/**
	 * Generate a random figure.
	 */
	public boolean randomFigure() {
		IGenerateFigureStrategy strategy = new GenerateRandomFigureStrategy(figure, this, gui);
		figureGenerator.setBehavior(strategy);
		return figureGenerator.concreteGenerate();
	}

	/**
	 * Undo command.
	 */
	public void undo() {
		if (currentCommandIndex >= 0 && currentCommandIndex <= commandHistory.size()) {
			commandHistory.get(currentCommandIndex).undo();
			currentCommandIndex--;
		}
	}

	/**
	 * Redo command.
	 */
	public void redo() {
		if (currentCommandIndex >= -1 && currentCommandIndex < commandHistory.size() - 1) {
			currentCommandIndex++;
			commandHistory.get(currentCommandIndex).execute();
		}
	}

	/**
	 * Store and execute command.
	 */
	private void storeAndExecuteCommand(ICommand command) {
		if (command != null) {
			currentCommandIndex++;
			commandHistory.subList(currentCommandIndex, commandHistory.size()).clear();
			commandHistory.add(command);
			command.execute();
		}
	}

	/**
	 * Reset figure.
	 */
	public void resetFigure() {
		setFigure(FigurePrototypes.createAndInit());
	}

	/**
	 * Get Cartman.
	 */
	public void getCartman() {
		setFigure(FigurePrototypes.getCartman());
	}

	/**
	 * Get Kenny.
	 */
	public void getKenny() {
		setFigure(FigurePrototypes.getKenny());
	}

	/**
	 * Get Stan.
	 */
	public void getStan() {
		setFigure(FigurePrototypes.getStan());
	}

	/**
	 * Get Kyle.
	 */
	public void getKyle() {
		setFigure(FigurePrototypes.getKyle());
	}

	/**
	 * Move selected body part on figure image.
	 */
	public void moveSelectedBodyPartOnImage(int num, Command action) {
		gui.getRadios().forEach((bodyPart, radioButton) -> {
			if (radioButton.isSelected()) {
				movePartOnImage(bodyPart, num, action);
			}
		});
	}

	/**
	 * Move body part on figure.
	 */
	private void movePartOnImage(BodyPart bodyPart, int num, Command action) {
		ICommand command = null;
		FigureElement figureElement = (FigureElement) figure.getElements().get(bodyPart.ordinal());

		switch (action) {
			case LEFT:
			case RIGHT:
			case UP:
			case DOWN:
				command = new MoveCommand(figureElement, action, num);
				break;

			case SCALE_UP:
			case SCALE_DOWN:
				command = new ScaleCommand(figureElement, action, num);
				break;

			default:
				log.error("Unknown action");
				return;
		}

		storeAndExecuteCommand(command);
	}
	
	public void refetch(BodyPart part){
		BodyPartPool poolToReload = getBodyPartPoolByBodyPart(part);
		poolToReload.reFetchImages();
	}
}
