package southpark.figure.controller;

import southpark.figure.enums.BodyPart;
import southpark.figure.enums.Command;
import southpark.figure.pool.BodyPartPool;

public interface IController {
	/**
	 * Get body part pool by body part.
	 */
	public BodyPartPool getBodyPartPoolByBodyPart(BodyPart part);

	/**
	 * Next body part.
	 */
	public void move(BodyPart bodyPart, Command action);

	/**
	 * Save figure image as PNG.
	 */
	public boolean saveImageAsPNG(String fileName);

	/**
	 * Save figure image as black & white PNG.
	 */
	public boolean saveImageAsMelancholicPNG(String fileName);

	/**
	 * Save figure image as figure.
	 */
	public boolean saveImageAsFigure(String fileName);

	/**
	 * Save figure image as CSV.
	 */
	public boolean saveImageAsCSV(String fileName);

	/**
	 * Load figure from file.
	 */
	public boolean loadFigureFromFile(String loadPathString);

	/**
	 * Generate a random figure.
	 */
	public boolean randomFigure();

	/**
	 * Undo command.
	 */
	public void undo();

	/**
	 * Redo command.
	 */
	public void redo();

	/**
	 * Reset figure.
	 */
	public void resetFigure();

	/**
	 * Get Cartman.
	 */
	public void getCartman();

	/**
	 * Get Kenny.
	 */
	public void getKenny();

	/**
	 * Get Stan.
	 */
	public void getStan();

	/**
	 * Get Kyle.
	 */
	public void getKyle();

	/**
	 * Move selected body part on figure image.
	 */
	public void moveSelectedBodyPartOnImage(int num, Command action);
	
	/**
	 * Refetch images
	 * @param partName
	 */
	public void refetch(BodyPart partName);
}
