package southpark.figure.pool;

import java.util.List;
import java.util.Random;

import southpark.figure.entity.FigureElement;
import southpark.figure.enums.BodyPart;
import southpark.figure.utils.Utils;
import southpark.figure.visitors.IVisitable;
import southpark.figure.visitors.IVisitor;

/**
 * Body part pool
 */
public class BodyPartPool implements IPool<FigureElement>, IVisitable<BodyPartPool> {
	/**
	 * Directory of Pool
	 */
	private String directory;
	
	/**
	 * Type of pool
	 */
	private BodyPart type;
	
	/**
	 * List of elements
	 */
	private List<FigureElement> elements;
	
	/**
	 * Counter
	 */
	private int counter = 0;

	/**
	 * Random number generator, for getRandom() method
	 */
	private static final Random rand = new Random();

	/**
	 * Class constructor.
	 */
	public BodyPartPool(String directory, BodyPart type) {
		this.directory = directory;
		this.type = type;
		reFetchImages();
	}

	/**
	 * Get type.
	 */
	public BodyPart getType() {
		return type;
	}

	/**
	 * Get next.
	 */
	public FigureElement getNext() {
		if (counter == elements.size() - 1) {
			counter = 0;
		} else {
			counter++;
		}
		return elements.get(counter);

	}

	/**
	 * Get previous.
	 */
	public FigureElement getPrevious() {
		if (counter == 0) {
			counter = elements.size() - 1;
		} else {
			counter--;
		}
		return elements.get(counter);
	}

	/**
	 * Get size.
	 */
	public int getSize() {
		return elements.size();
	}

	/**
	 * Get random.
	 */
	public FigureElement getRandom() {
		if (elements.isEmpty()) {
			return null;
		}
		return elements.get(rand.nextInt(elements.size()));
	}

	/**
	 * Accept a BodyPartPool visitor.
	 */
	public void accept(IVisitor<BodyPartPool> visitor) {
		visitor.visit(this);
	}

	/**
	 * Refetch images.
	 */
	public final void reFetchImages() {
		this.elements = Utils.fetchImages(directory);
	}

}
