package southpark.figure.entity;

import southpark.figure.gui.ImagePanel;
import southpark.figure.visitors.IVisitor;

/**
 * Figure interface
 */
public interface IFigure extends Cloneable {

	/**
	 * Get element image.
	 */
	public ImagePanel getElementImage();
	
	/**
	 * Get coolness.
	 */
	public int getCoolness();
	
	public void initElementImage();
	
	/**
	 * Accept an IFigure visitor.
	 */
	public void accept(IVisitor<IFigure> viv);
	
}
