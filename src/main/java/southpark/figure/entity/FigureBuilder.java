package southpark.figure.entity;

import southpark.figure.enums.BodyPart;
import southpark.figure.utils.Constants;

/**
 * Figure builder class
 */
public class FigureBuilder {
	
	/**
	 * Concatenate images
	 * @param dir
	 * @param image
	 * @return
	 */
	private static String concatenate(String dir, String image){
		if(dir != null && image != null){
			return dir + "/" + image;
		}
		return "";
	}
	
	/**
	 * Hat
	 */
	private FigureElement hat = new FigureElement(concatenate(Constants.HATS_DIR, Constants.BLANK_IMAGE), Constants.DEFAULT_VALUE);
	
	/**
	 * Hair
	 */
	private FigureElement hair = new FigureElement(concatenate(Constants.HAIR_DIR, Constants.BLANK_IMAGE), Constants.DEFAULT_VALUE);
	
	/**
	 * Eyes
	 */
	private FigureElement eyes = new FigureElement(concatenate(Constants.EYES_DIR, "eyes_1.png"), Constants.DEFAULT_VALUE);
	
	/**
	 * Mouth
	 */
	private FigureElement mouth = new FigureElement(concatenate(Constants.MOUTH_DIR, "mouth_1.png"), Constants.DEFAULT_VALUE);
	
	/**
	 * Hands
	 */
	private FigureElement hands = new FigureElement(concatenate(Constants.HANDS_DIR, Constants.BLANK_IMAGE), Constants.DEFAULT_VALUE);
	
	/**
	 * Legs
	 */
	private FigureElement legs = new FigureElement(concatenate(Constants.LEGS_DIR, "legs_5.png"), Constants.DEFAULT_VALUE);
	
	/**
	 * Shoes
	 */
	private FigureElement shoes = new FigureElement(concatenate(Constants.SHOES_DIR, "shoes_20.png"), Constants.DEFAULT_VALUE);
	
	/**
	 * Accessories
	 */
	private FigureElement accessories = new FigureElement(concatenate(Constants.ACCESSORIES_DIR, Constants.BLANK_IMAGE), Constants.DEFAULT_VALUE);
	
	/**
	 * Shoes
	 */
	private FigureElement shirt = new FigureElement(concatenate(Constants.SHIRT_DIR, "butters_15.png"), Constants.DEFAULT_VALUE);
	
	/**
	 * Skin
	 */
	private FigureElement skin = new FigureElement(concatenate(Constants.SKIN_DIR, "skin_1.png"), Constants.DEFAULT_VALUE);
	
	/**
	 * Background
	 */
	private FigureElement background = new FigureElement(concatenate(Constants.BACKGROUND_DIR, "bg_2.png"), Constants.DEFAULT_VALUE);

	/**
	 * Prop
	 */
	private FigureElement prop = new FigureElement(concatenate(Constants.PROPS_DIR, "cat_30.png"), Constants.DEFAULT_VALUE);

	/**
	 * With hat.
	 */
	public FigureBuilder withHat(FigureElement hat) {
		this.hat = hat;
		return this;
	}

	/**
	 * With hair.
	 */
	public FigureBuilder withHair(FigureElement hair) {
		this.hair = hair;
		return this;
	}

	/**
	 * With eyes.
	 */
	public FigureBuilder withEyes(FigureElement eyes) {
		this.eyes = eyes;
		return this;
	}

	/**
	 * With mouth.
	 */
	public FigureBuilder withMouth(FigureElement mouth) {
		this.mouth = mouth;
		return this;
	}

	/**
	 * With hands.
	 */
	public FigureBuilder withHands(FigureElement hands) {
		this.hands = hands;
		return this;
	}

	/**
	 * With legs.
	 */
	public FigureBuilder withLegs(FigureElement legs) {
		this.legs = legs;
		return this;
	}

	/**
	 * With shoes.
	 */
	public FigureBuilder withShoes(FigureElement shoes) {
		this.shoes = shoes;
		return this;
	}

	/**
	 * With prop.
	 */
	public FigureBuilder withProp(FigureElement prop) {
		this.prop = prop;
		return this;
	}

	/**
	 * With accessories.
	 */
	public FigureBuilder withAccessories(FigureElement accessories) {
		this.accessories = accessories;
		return this;
	}

	/**
	 * With shirt.
	 */
	public FigureBuilder withShirt(FigureElement shirt) {
		this.shirt = shirt;
		return this;
	}

	/**
	 * With skin.
	 */
	public FigureBuilder withSkin(FigureElement skin) {
		this.skin = skin;
		return this;
	}

	/**
	 * With background.
	 */
	public FigureBuilder withBackground(FigureElement background) {
		this.background = background;
		return this;
	}

	/**
	 * Build.
	 */
	public Figure build() {
		Figure figure = new Figure();
		figure.add(BodyPart.PROPS, prop);
		figure.add(BodyPart.HAT, hat);
		figure.add(BodyPart.HAIR, hair);
		figure.add(BodyPart.EYES, eyes);
		figure.add(BodyPart.MOUTH, mouth);
		figure.add(BodyPart.HANDS, hands);
		figure.add(BodyPart.LEGS, legs);
		figure.add(BodyPart.SHOES, shoes);
		figure.add(BodyPart.ACCESSORIES, accessories);
		figure.add(BodyPart.SHIRT, shirt);
		figure.add(BodyPart.SKIN, skin);
		figure.add(BodyPart.BACKGROUND, background);
		return figure;
	}
}
