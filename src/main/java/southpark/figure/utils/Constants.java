package southpark.figure.utils;

import java.awt.Dimension;

/**
 * Constants container
 */
public final class Constants {
	/**
	 * Figure panel's dimension
	 */
	public static final Dimension windowSize = new Dimension(490, 550);

	/**
	 * Extensions we can use, and we allow to use
	 */
	public static final String[] SUPPORTED_EXTENSIONS = { "jpg", "png" };
	public static final String SAVE_SUFFIX = ".figure";
	public static final String IMG_SUFFIX = ".jpg";
	public static final String CSV_SUFFIX = ".csv";

	/**
	 * Resources directory
	 */
	public static final String RESOURCES_DIR = "resources";

	/**
	 * Blank image directory
	 */
	public static final String BLANK_IMAGE = "blank.png";
	/**
	 * Images directory
	 */
	public static final String BACKGROUND_DIR = "resources/background";
	public static final String HATS_DIR = "resources/hat";
	public static final String HAIR_DIR = "resources/hair";
	public static final String EYES_DIR = "resources/eyes";
	public static final String HANDS_DIR = "resources/hands";
	public static final String LEGS_DIR = "resources/legs";
	public static final String MOUTH_DIR = "resources/mouth";
	public static final String ACCESSORIES_DIR = "resources/accessories";
	public static final String SHIRT_DIR = "resources/shirt";
	public static final String SHOES_DIR = "resources/shoes";
	public static final String SKIN_DIR = "resources/skin";
	public static final String PROPS_DIR = "resources/props";

	/**
	 * How many body parts we need
	 */
	public static final int PARTS_SIZE = 11;

	/**
	 * Default value for Builder's default body parts.
	 */
	public static final int DEFAULT_VALUE = 0;

	/**
	 * Default value for moving props
	 */
	public static final int DEFAULT_STEP = 10;
	public static final double DEFAULT_SIZE_MOD = 0.1;
	public static final String IMAGE_EXTENSION = "jpg";

	/**
	 * Some language phrases. Preparation for multi lang support
	 */
	public static final String FAILED_TO_WRITE_IMAGE = "Failed to write image";
	public static final String IMAGE_CREATED_SUCCESFULLY = "Image created succesfully";
	public static final String SPECIFY_A_FILE_TO_SAVE = "Specify a file to save";
}