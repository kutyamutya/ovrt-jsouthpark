package southpark.figure.drawing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

/**
 * Circle
 */
public class DrawCircle extends Drawable implements Cloneable {

	/**
	 * Constructor
	 */
	public DrawCircle() {
		super();
	}

	/**
	 * Constructor
	 */
	public DrawCircle(Color foreground, Point point, int size) {
		super(foreground, point, size);
	}

	@Override
	public void drawShape(Graphics g) {
		g.drawOval(((int) location.getX()), ((int) location.getY()), size, size);

	}

	@Override
	public Drawable clone() throws CloneNotSupportedException { // NOPMD
		return new DrawCircle(this.color, this.location, this.size);
	}

}
