package southpark.figure.drawing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

/**
 * Draws point
 */
public class DrawPoint extends Drawable implements Cloneable {
	/**
	 * Constructor
	 */
	public DrawPoint() {
		super();
	}

	/**
	 * Constructor
	 */
	public DrawPoint(Color foreground, Point point, int size) {
		super(foreground, point, size);
	}

	@Override
	public void drawShape(Graphics g) {
		g.fillOval(((int) location.getX()), ((int) location.getY()), size, size);

	}

	@Override
	public Drawable clone() throws CloneNotSupportedException { // NOPMD
		return new DrawPoint(this.color, this.location, this.size);
	}

}
