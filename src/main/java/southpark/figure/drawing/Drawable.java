package southpark.figure.drawing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

/**
 * Drawable abstract class - State pattern
 */
public abstract class Drawable implements Cloneable {
	/**
	 * Color that we want to use to draw
	 */
	protected Color color;

	/**
	 * Location where draw will start
	 */
	protected Point location;

	/**
	 * Shape's size
	 */
	protected int size = 10;

	/**
	 * Constructor
	 * 
	 * @param color
	 *            Color that we want to use to draw
	 * @param location
	 *            Location where draw will start
	 * @param size
	 *            Shape's size
	 */
	public Drawable(Color color, Point location, int size) {
		super();
		this.color = color;
		this.location = location;
		this.size = size;
		centerize(); //NOPMD
	}

	/**
	 * This method centerize the location of the point we've clicked. After
	 * applying this method, the shape is centered to the clicked point.
	 */
	protected void centerize() {
		double oldX = location.getX();
		double oldY = location.getY();
		double offset = size / 2.0;
		this.location = new Point((int) (oldX - offset), (int) (oldY - offset));
	}

	/**
	 * Constrcutor
	 * @param color
	 * @param location
	 */
	public Drawable(Color color, Point location) {
		super();
		this.color = color;
		this.location = location;
		this.size = 10;
	}

	/**
	 * Constructor
	 */
	public Drawable() {
		super();
	}

	public abstract Drawable clone() throws CloneNotSupportedException;

	/**
	 * The drawing method that needs to be implemented by subclasses according
	 * to the shape we want to use
	 * 
	 * @param graphics
	 */
	public abstract void drawShape(Graphics graphics);

	/**
	 * Concrete drawing method
	 * 
	 * @param g
	 *            graphics
	 */
	public void concreteDrawShape(Graphics g) {
		g.setColor(color);
		drawShape(g);
	}

	/**
	 * @return the color
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * @param color
	 *            the color to set
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * @return the location
	 */
	public Point getLocation() {
		return location;
	}

	/**
	 * @param location
	 *            the location to set
	 */
	public void setLocation(Point location) {
		this.location = location;
	}

	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}

	/**
	 * @param size
	 *            the size to set
	 */
	public void setSize(int size) {
		this.size = size;
	}

}