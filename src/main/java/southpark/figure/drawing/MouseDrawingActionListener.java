package southpark.figure.drawing;

import java.awt.event.MouseEvent;

import javax.swing.event.MouseInputAdapter;

import southpark.figure.gui.DrawPanel;
import southpark.figure.utils.Utils;

/**
 * Mouse draw listener
 */
public class MouseDrawingActionListener extends MouseInputAdapter {
	/**
	 * panel we are listening
	 */
	private DrawPanel panel;

	/**
	 * Constructor
	 */
	public MouseDrawingActionListener(DrawPanel drawPanel) {
		this.panel = drawPanel;
	}

	/**
	 * Mouse pressed event
	 */
	public void mousePressed(MouseEvent e) {
		Drawable currentDrawingObject = panel.getCurrentState();

		currentDrawingObject.setSize(Utils.parseInteger(panel.getSizeField()));
		currentDrawingObject.setColor(e.getComponent().getForeground());
		currentDrawingObject.setLocation(e.getPoint());
		panel.getDrawingArea().addPoint(currentDrawingObject);
	}

}