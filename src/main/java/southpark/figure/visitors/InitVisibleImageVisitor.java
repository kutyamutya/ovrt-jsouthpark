package southpark.figure.visitors;

import java.util.ArrayList;
import java.util.List;

import southpark.figure.entity.IFigure;
import southpark.figure.gui.ImagePanel;

/**
 * Inits visible image
 * 
 * This visitor inits the visible image.
 * Containing them in a list, which will be processed
 * 
 * Implementation of IVisitor interface
 * 
 * This implementation is implements IVisitor for IFigure
 * 
 *
 */
public class InitVisibleImageVisitor implements IVisitor<IFigure> {

	private List<ImagePanel> elements = new ArrayList<>();
	
	/**
	 * Visit.
	 */
	public void visit(IFigure figure) {
		elements.add(figure.getElementImage());
	}

	/**
	 * Get elements.
	 */
	public List<ImagePanel> getElements(){
		return elements;
	}
	
}
