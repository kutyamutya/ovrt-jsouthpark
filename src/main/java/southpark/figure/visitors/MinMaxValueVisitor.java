package southpark.figure.visitors;

import southpark.figure.entity.IFigure;

/**
 * Minimum and maximum values visitor
 * 
 * Searches for the minimum and maximum element of the given container
 * 
 * Implementation of IVisitor interface
 * 
 * This implementation is implements IVisitor for IFigure
 * 
 * At each step, we check whether the current element is greater than the
 * current greatest, or lower than the current lowest
 * 
 * If one of this is true, we change the current greatest/lowest element to the
 * current element
 * 
 */
public class MinMaxValueVisitor implements IVisitor<IFigure> {

	int minCoolness = Integer.MAX_VALUE;
	int maxCoolness = Integer.MIN_VALUE;
	IFigure min = null;
	IFigure max = null;

	/**
	 * Visit.
	 */
	public void visit(IFigure figure) {
		int currentCoolness = figure.getCoolness();
		if (currentCoolness != 0 && (currentCoolness < minCoolness)) {
			min = figure;
			minCoolness = currentCoolness;
		}
		if (currentCoolness > maxCoolness) {
			max = figure;
			maxCoolness = currentCoolness;
		}
	}

	/**
	 * Get minimum coolness.
	 */
	public int getMinCoolness() {
		return minCoolness;
	}

	/**
	 * Get maximum coolness.
	 */
	public int getMaxCoolness() {
		return maxCoolness;
	}

	/**
	 * Get minimum.
	 */
	public IFigure getMin() {
		return min;
	}

	/**
	 * Get maximum.
	 */
	public IFigure getMax() {
		return max;
	}

}
