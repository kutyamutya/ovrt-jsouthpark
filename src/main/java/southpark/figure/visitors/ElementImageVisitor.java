package southpark.figure.visitors;

import southpark.figure.entity.IFigure;

/**
 * 
 * Element Image visitor
 * 
 * Implementation of IVisitor interface
 * 
 * This implementation is implements IVisitor for IFigure
 * 
 *
 * Required for saving and loading image to our .figure extension. Basically,
 * it's only JSON :)
 * 
 * When an IFigure is visited, its element image (which is ImagePanel) gets
 * initialized, if it is not.
 *
 */
public class ElementImageVisitor implements IVisitor<IFigure> {

	/**
	 * Visit.
	 */
	public void visit(IFigure figure) {
		figure.initElementImage();
	}

}
