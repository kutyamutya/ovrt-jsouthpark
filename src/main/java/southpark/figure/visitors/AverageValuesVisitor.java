package southpark.figure.visitors;

import southpark.figure.entity.IFigure;

/**
 * Average values
 * 
 * Average values of the given container
 * 
 * Implementation of IVisitor interface
 * 
 * This implementation is implements IVisitor for IFigure
 * 
 *
 * The algorithm:
 * 
 * 	- Count the values of the container as sum
 *  - Count the number of elements as count
 *  
 *  - Result :   sum
 *  		   -------
 *              count
 * 
 */
public class AverageValuesVisitor implements IVisitor<IFigure> {

	private int sum = 0;
	private int count = 0;

	/**
	 * Visit.
	 */
	public void visit(IFigure figure) {
		sum += figure.getCoolness();
		count++;
	}

	/**
	 * Get average.
	 */
	public double getAvarage() {
		double roundedNumber = (double) sum / count;
		roundedNumber = Math.round(roundedNumber * 100);
		roundedNumber = roundedNumber / 100;
		return roundedNumber;
	}

	/**
	 * Get sum.
	 */
	public int getSum() {
		return sum;
	}

	/**
	 * Get count.
	 */
	public int getCount() {
		return count;
	}

}
