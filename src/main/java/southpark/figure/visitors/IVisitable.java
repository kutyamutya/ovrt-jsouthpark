package southpark.figure.visitors;

/**
 * Public interface for visitable objects
 * 
 * @param <T>
 * 			Type that we will want to visit
 */
public interface IVisitable<T> {

	/**
	 * Accept a generic visitor.
	 */
	public void accept(IVisitor<T> visitor);

}
