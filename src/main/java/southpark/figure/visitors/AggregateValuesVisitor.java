package southpark.figure.visitors;

import southpark.figure.entity.IFigure;

/**
 * Aggregate values
 * 
 * Aggregates values of the given container
 * 
 * Implementation of IVisitor interface
 * 
 * This implementation is implements IVisitor for IFigure
 * 
 *
 */
public class AggregateValuesVisitor implements IVisitor<IFigure> {

	private int value = 0;

	/**
	 * Visit.
	 */
	public void visit(IFigure figure) {
		if (figure != null) {
			value += figure.getCoolness();
		}
	}

	/**
	 * Get value.
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Reset visitor.
	 */
	public void resetVisitor() {
		this.value = 0;
	}

}
