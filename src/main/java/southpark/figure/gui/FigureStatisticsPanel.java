package southpark.figure.gui;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import southpark.figure.entity.Figure;
import southpark.figure.observer.IObservable;
import southpark.figure.observer.IObserver;

/**
 * Figure statistics panel
 * @author kutyamutya
 *
 */
public class FigureStatisticsPanel extends JPanel implements IObserver {

	private static final long serialVersionUID = -7852707412108503654L;

	private JLabel avgValue;
	private JLabel minValue;
	private JLabel maxValue;
	private JLabel sumValue;

	/**
	 * Class constructor.
	 */
	public FigureStatisticsPanel() {
		super(new GridLayout(4, 2, 10, 0));
		setBackground(new Color(0, 0, 0, 75));
		setBorder(new EmptyBorder(10, 10, 10, 10));

		JLabel avgLabel = new JLabel("Avarage coolness: ");
		avgLabel.setForeground(Color.white);
		avgLabel.setHorizontalAlignment(JLabel.RIGHT);
		add(avgLabel);

		avgValue = new JLabel(String.valueOf("0"));
		avgValue.setForeground(Color.white);
		add(avgValue);

		JLabel minLabel = new JLabel("Minimum coolness: ");
		minLabel.setForeground(Color.white);
		minLabel.setHorizontalAlignment(JLabel.RIGHT);
		add(minLabel);

		minValue = new JLabel(String.valueOf("0"));
		minValue.setForeground(Color.white);
		add(minValue);

		JLabel maxLabel = new JLabel("Maximum coolness: ");
		maxLabel.setForeground(Color.white);
		maxLabel.setHorizontalAlignment(JLabel.RIGHT);
		add(maxLabel);

		maxValue = new JLabel(String.valueOf("0"));
		maxValue.setForeground(Color.white);
		add(maxValue);

		JLabel sumLabel = new JLabel("Aggregated coolness: ");
		sumLabel.setForeground(Color.white);
		sumLabel.setHorizontalAlignment(JLabel.RIGHT);
		add(sumLabel);

		sumValue = new JLabel(String.valueOf("0"));
		sumValue.setForeground(Color.white);
		add(sumValue);
	}

	/**
	 * Update statistics frame.
	 */
	public void update(IObservable observable) {
		if (observable instanceof Figure) {
			Figure figure = (Figure) observable;
			minValue.setText(String.valueOf(figure.getMinCoolness()) + " (" + figure.getMinItemName() + ")");
			maxValue.setText(String.valueOf(figure.getMaxCoolness())+ " (" + figure.getMaxItemName() + ")");
			avgValue.setText(String.valueOf(figure.getAvgCoolness()));
			sumValue.setText(String.valueOf(figure.getCoolness()));
		}
	}

}
