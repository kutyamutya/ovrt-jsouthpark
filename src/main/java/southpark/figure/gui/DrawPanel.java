package southpark.figure.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Locale;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.log4j.Logger;

import southpark.figure.controller.IController;
import southpark.figure.drawing.DrawCircle;
import southpark.figure.drawing.DrawPoint;
import southpark.figure.drawing.DrawRectangle;
import southpark.figure.drawing.DrawTriangle;
import southpark.figure.drawing.Drawable;
import southpark.figure.drawing.MouseDrawingActionListener;
import southpark.figure.enums.BodyPart;
import southpark.figure.utils.Utils;

/**
 * Draw panel
 */
public class DrawPanel {

	private static final Logger log = Logger.getLogger(DrawPanel.class.getCanonicalName());

	private JFrame frame;
	private Drawable currentState;

	private DrawingArea drawingArea = new DrawingArea();
	private JPanel controllerPanel = new JPanel(new GridLayout(4, 1, 0, 0));

	private JTextField sizeField = new JTextField("20");
	private JTextField coolnessField = new JTextField("50");
	private JComboBox<BodyPart> bodyPart = new JComboBox<>(BodyPart.values());
	private IController controller;

	/**
	 * Constructor
	 */
	public DrawPanel(IController controller) {
		this.controller = controller;
		sizeField.setPreferredSize(new Dimension(50, 20));
		coolnessField.setPreferredSize(new Dimension(50, 20));
		setCurrentState(new DrawPoint());
		initControllerPanel();
	}

	/**
	 * Returns the state
	 * 
	 * @return
	 */
	public Drawable getCurrentState() {
		return currentState;
	}

	/**
	 * Sets the state
	 * 
	 * @param currentState
	 */
	public void setCurrentState(Drawable currentState) {
		this.currentState = currentState;
	}

	private JPanel initStatePanel() {
		JPanel statePanel = new JPanel();
		statePanel.add(createButton("Point", event -> {
			setCurrentState(new DrawPoint());
		}));
		statePanel.add(createButton("Circle", event -> {
			setCurrentState(new DrawCircle());
		}));
		statePanel.add(createButton("Rectangle", event -> {
			setCurrentState(new DrawRectangle());
		}));
		statePanel.add(createButton("Triangle", event -> {
			setCurrentState(new DrawTriangle());
		}));

		return statePanel;

	}

	private void initControllerPanel() {
		controllerPanel.add(initStatePanel());
		controllerPanel.add(initColorPanel());
		controllerPanel.add(initButtonPanel());
		controllerPanel.add(initClearSavePanel());
	}

	private JPanel initClearSavePanel() {
		JPanel clearSave = new JPanel();
		clearSave.add(createButton("Clear Drawing", null, event -> {
			drawingArea.clear();

		}));

		clearSave.add(bodyPart);

		clearSave.add(createButton("Save", null, event -> {
			saveImage();
		}));

		return clearSave;
	}

	/**
	 * Saves images
	 */
	private void saveImage() {
		BufferedImage bi = new BufferedImage(drawingArea.getSize().width, drawingArea.getSize().height, BufferedImage.TYPE_INT_ARGB);
		Graphics g = bi.createGraphics();
		drawingArea.setBorder(BorderFactory.createEmptyBorder());
		drawingArea.paint(g);
		drawingArea.setBorder(BorderFactory.createLineBorder(Color.black));
		g.dispose();
		String selectedItem = ((BodyPart) bodyPart.getSelectedItem()).name().toLowerCase(Locale.getDefault());
		try {
			int coolness = Utils.parseInteger(coolnessField);
			ImageIO.write(bi, "png", new File("resources/" + selectedItem + "/test" + Utils.randomInteger(1500000) + "_" + coolness + ".png"));
			controller.refetch((BodyPart) bodyPart.getSelectedItem());
			// JOptionPane.showMessageDialog(null,
			// "You have to restart the program to use your new " + selectedItem
			// + "!", "Please restart", JOptionPane.INFORMATION_MESSAGE);
			frame.dispose();
		} catch (IOException e) {
			log.error("Failed to save image" + e);
		}
	}

	private JButton createButton(String text, ActionListener action) {
		return createButton(text, null, action);
	}

	private JButton createButton(String text, Color background) {
		JButton button = new JButton(text);
		button.setBackground(background);

		button.addActionListener(event -> {
			drawingArea.setForeground(button.getBackground());
		});

		return button;

	}

	private JButton createButton(String text, Color background, ActionListener actionListener) {
		JButton button = new JButton(text);
		button.setBackground(background);
		button.addActionListener(actionListener);

		return button;
	}

	private JPanel initColorPanel() {
		JPanel colorPanel = new JPanel();
		colorPanel.add(createButton("	", Color.WHITE));
		colorPanel.add(createButton("	", Color.BLACK));
		colorPanel.add(createButton("	", Color.RED));
		colorPanel.add(createButton("	", Color.GREEN));
		colorPanel.add(createButton("	", Color.BLUE));
		colorPanel.add(createButton("	", Color.ORANGE));
		colorPanel.add(createButton("	", Color.YELLOW));
		colorPanel.add(createButton("	", Color.CYAN));
		colorPanel.add(createButton("	", Color.GRAY));
		colorPanel.add(createButton("	", Color.MAGENTA));
		colorPanel.add(createButton("	", Color.PINK));

		return colorPanel;
	}

	private JPanel initButtonPanel() {
		JPanel buttonPanel = new JPanel();

		buttonPanel.add(new JLabel("Size of the brush:"));
		buttonPanel.add(sizeField);

		buttonPanel.add(new JLabel("Coolness value:"));
		buttonPanel.add(coolnessField);

		return buttonPanel;
	}

	public void createAndShowGUI() {

		drawingArea.addMouseListener(new MouseDrawingActionListener(this));

		frame = new JFrame("Draw your own body part");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new FlowLayout());
		frame.getContentPane().add(drawingArea);
		frame.getContentPane().add(controllerPanel);
		frame.setSize(600, 750);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	/**
	 * @return the drawingArea
	 */
	public DrawingArea getDrawingArea() {
		return drawingArea;
	}

	/**
	 * @param drawingArea
	 *            the drawingArea to set
	 */
	public void setDrawingArea(DrawingArea drawingArea) {
		this.drawingArea = drawingArea;
	}

	/**
	 * @return the sizeField
	 */
	public JTextField getSizeField() {
		return sizeField;
	}

	/**
	 * @param sizeField
	 *            the sizeField to set
	 */
	public void setSizeField(JTextField sizeField) {
		this.sizeField = sizeField;
	}

}