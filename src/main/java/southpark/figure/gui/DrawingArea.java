package southpark.figure.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import org.apache.log4j.Logger;

import southpark.figure.drawing.Drawable;
import southpark.figure.utils.Constants;

/**
 * Drawing area implementation
 */
public class DrawingArea extends JPanel {
	private static final long serialVersionUID = 1843274037621757851L;

	/**
	 * List of elements
	 */
	private List<Drawable> drawables = new ArrayList<>();

	/**
	 * Current state
	 */
	private Drawable currentState;

	/**
	 * Constructor
	 */
	public DrawingArea() {
		this.setOpaque(false);
		this.setBorder(BorderFactory.createLineBorder(Color.black));
		this.setPreferredSize(new Dimension(Constants.windowSize.width, Constants.windowSize.height));
		this.setMinimumSize(new Dimension(Constants.windowSize.width, Constants.windowSize.height));
		this.setMaximumSize(new Dimension(Constants.windowSize.width, Constants.windowSize.height));
		this.setSize(new Dimension(Constants.windowSize.width, Constants.windowSize.height));
	}

	/**
	 * Sets the current state of the drawable
	 * 
	 * @param currentState
	 */
	public void setCurrentStat(Drawable currentState) {
		this.currentState = currentState;
	}

	/**
	 * Paints component
	 */
	@Override
	protected void paintComponent(Graphics g) {
		drawables.forEach(item -> {
			item.concreteDrawShape(g);
		});
	}

	/**
	 * Adds point
	 * 
	 * @param pt
	 *            point to add
	 */
	public void addPoint(Drawable pt) {
		try {
			drawables.add(pt.clone());
		} catch (CloneNotSupportedException exception) {
			Logger.getLogger(DrawingArea.class.getCanonicalName()).error("Cannot clone: " + exception);
		}
		repaint();
	}

	/**
	 * Clears drawing area
	 */
	public void clear() {
		drawables.clear();
		repaint();
	}

	/**
	 * Gets the drawables
	 * 
	 * @return drawables
	 */
	public List<Drawable> getDrawables() {
		return drawables;
	}

	/**
	 * Sets the drawables
	 * 
	 * @param drawables
	 */
	public void setDrawables(List<Drawable> drawables) {
		this.drawables = drawables;
	}

	/**
	 * Returns with the current state
	 * 
	 * @return
	 */
	public Drawable getCurrentState() {
		return currentState;
	}

	/**
	 * Sets the current state
	 * 
	 * @param currentState
	 */
	public void setCurrentState(Drawable currentState) {
		this.currentState = currentState;
	}

}