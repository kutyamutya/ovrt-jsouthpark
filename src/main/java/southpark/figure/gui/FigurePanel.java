package southpark.figure.gui;

import javax.swing.JLayeredPane;

import southpark.figure.entity.Figure;
import southpark.figure.observer.IObservable;
import southpark.figure.observer.IObserver;

/**
 * Figure panel
 * @author kutyamutya
 *
 */
public class FigurePanel extends JLayeredPane implements IObserver {

	private static final long serialVersionUID = 7108468549419046417L;

	/**
	 * Update figure pane.
	 */
	public void update(IObservable observable) {
		if (observable instanceof Figure) {
			revalidate();
			repaint();
		}
	}

}
