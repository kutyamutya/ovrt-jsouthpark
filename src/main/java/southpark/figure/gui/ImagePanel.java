package southpark.figure.gui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.imgscalr.Scalr;

/**
 * Image panel for body parts images
 */
public class ImagePanel extends JPanel {

	private static final long serialVersionUID = 8493986947358722824L;
	private static final Logger log = Logger.getLogger(ImagePanel.class.getCanonicalName());

	/**
	 * Image
	 */
	private BufferedImage img;

	/**
	 * Original image
	 */
	private BufferedImage imgOrig;

	/**
	 * Image path
	 */
	private String path;

	/**
	 * Class constructor.
	 */
	public ImagePanel(String img) {
		this(new File(img));
	}

	/**
	 * Class constructor.
	 */
	public ImagePanel(File file) {
		path = file.getAbsolutePath();
		try {
			img = ImageIO.read(new File(path));
			// Initialize original image as null, it will be instantiated when
			// necessary.
			imgOrig = null;
		} catch (IOException e) {
			log.error(e);
		}
		Dimension size = new Dimension(img.getWidth(), img.getHeight());
		setPreferredSize(size);
		setMinimumSize(size);
		setMaximumSize(size);
		setSize(size);
		setLayout(null);
	}

	/**
	 * Class constructor.
	 */
	public ImagePanel(BufferedImage img) {
		path = "Unknown, got image.";
		this.img = img;
		// Initialize original image as null, it will be instantiated when
		// necessary.
		imgOrig = null;
		Dimension size = new Dimension(img.getWidth(null), img.getHeight(null));
		setPreferredSize(size);
		setMinimumSize(size);
		setMaximumSize(size);
		setSize(size);
		setLayout(null);
	}

	/**
	 * Get image.
	 */
	public BufferedImage getImg() {
		// Create a copy from the image (the original instance won't be
		// modified).
		BufferedImage img = new BufferedImage(this.img.getWidth(), this.img.getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics g = img.createGraphics();
		g.drawImage(this.img, 0, 0, null);
		return img;
	}

	/**
	 * Scale (resize) image panel.
	 */
	public void scale(int num) {
		if (num >= 0) {
			// Save image as original if is haven't created yet.
			if (imgOrig == null) {
				imgOrig = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_ARGB);
				Graphics g = imgOrig.createGraphics();
				g.drawImage(img, 0, 0, null);
			}
			// Resize image. Work with the original image for the best result.
			BufferedImage img = Scalr.resize(imgOrig, num);
			// Set image.
			this.img = img;
			// Set size.
			setSize(new Dimension(this.img.getWidth(), this.img.getHeight()));
		}
	}

	/**
	 * Get path.
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Get container directory path.
	 */
	public String getContainerDirectoryPath() {
		final String[] splittedPath = path.split(Pattern.quote(File.separator));
		if (splittedPath.length > 2) {
			return StringUtils.capitalize(splittedPath[splittedPath.length-2]);
		}
		return "Unknown";
	}

	/**
	 * Set path.
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * Paint component.
	 */
	public void paintComponent(Graphics g) {
		g.drawImage(img, 0, 0, null);
	}

	/**
	 * To string.
	 */
	public String toString() {
		return path + " " + super.toString();
	}

}
